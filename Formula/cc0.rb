# C0 compiler and interpreter, used primarily by
# students at Carnegie Mellon University
class Cc0 < Formula
  desc "C0 compiler and interpreter"
  homepage "https://c0.cs.cmu.edu/"
  url "https://bitbucket.org/c0-lang/downloads/downloads/cc0-1.2.2.tgz"
  sha256 "26e612977654808f847de50131d836e0f5803a0033b0b1b62d967fa1aa08ac8a"
  license "BSD-2-Clause"

  depends_on "mlton" => :build

  # For the MLton runtime
  depends_on "gmp"
  # For the image manipulation library
  depends_on "libpng"

  def install
    Dir.chdir "cc0" do
      system "./configure"
      # Note we didn't write ENV.deparallelize,
      # so this 'make' will be parallel
      system "make"
      libexec.install %w[bin include runtime lib]

      # CC0 looks for the runtimes
      # in (dirname argv[0])/../runtime.
      # However this is not compatible
      # with how homebrew symlinks the binaries into /usr/local
      # Therefore we create this wrapper script
      (bin/"cc0").write <<~EOS
        #!/bin/sh
        #{libexec}/bin/cc0.bin $*
      EOS

      (bin/"coin").write <<~EOS
        #!/bin/sh
        #{libexec}/bin/coin.bin $*
      EOS
    end
  end

  test do
    # This tests the native dynamic-linked libs
    # from lib/, and the division tests runtime/libc0rt.dylib
    (testpath/"test.c0").write <<~EOS
      #use <string>

      int main() {
        string s = "Hello, world";
        int len = string_length(s);

        int i = 4 / 2;

        return len + i;
      }
    EOS

    system "#{bin}/cc0", "test.c0"
    # Note the C0 language prints the return value from main
    # and returns 0 to the shell if no runtime errors occur
    assert_match "14", shell_output("./a.out")
  end
end
